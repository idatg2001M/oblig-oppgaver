package org.oblig2;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * A super class for all members.
 */
public class BonusMember {
  private final double factorSilver = 1.2;
  private final double factorGold = 1.5;
  private final int memberNo;
  private final Personals personals;
  private final LocalDate enrolledDate;
  protected int bonuspoints = 0;

  /**
   * A constructor for BonusMember.
   *
   * @param memberNo     Member number
   * @param personals    Perosnal details
   * @param enrolledDate Date enrolled
   */
  public BonusMember(int memberNo, Personals personals, LocalDate enrolledDate) {
    this.memberNo = memberNo;
    this.personals = personals;
    this.enrolledDate = enrolledDate;
  }

  public double getFactorSilver() {
    return factorSilver;
  }

  public double getFactorGold() {
    return factorGold;
  }

  public int getMemberNo() {
    return memberNo;
  }

  public Personals getPersonals() {
    return personals;
  }

  public LocalDate getEnrolledDate() {
    return enrolledDate;
  }

  public int getPoints() {
    return bonuspoints;
  }

  /**
   * A method for checking if there has been more than 365 after they signed up.
   *
   * @param date Date to compare to the sign up date.
   * @return the amount of points the member gets
   */
  public int findQualificationPoints(LocalDate date) {
    long daysBetween = ChronoUnit.DAYS.between(this.enrolledDate, date);
    int points = 0;
    if (daysBetween < 365) {
      points = getPoints();
    }
    return points;
  }

  /**
   * Checks if the password is correct.
   *
   * @param password Password
   * @return true if password is correct, otherwise false.
   */
  public boolean okPassword(String password) {
    return personals.okPassword(password);
  }

  /**
   * A method to register points to a member.
   *
   * @param points Amount of points to be registered.
   */
  public void registerPoints(int points) {
    this.bonuspoints += points;
  }
}
