package org.oblig2;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Random;

/**
 * A class for archibing all the members.
 */
public class MemberArchive {
  private HashMap<Integer, BonusMember> members;

  /**
   * A constructor for MemberArchive.
   */
  public MemberArchive() {
    members = new HashMap<>();
  }

  /**
   * A methods whichs finds the amount of points based on the memberNo and password.
   *
   * @param memberNo Member number
   * @param password Password
   * @return amount of points the member has
   */
  public int findPoints(int memberNo, String password) {
    int returnPoints = -1;
    for (BonusMember member : members.values()) {
      if (memberNo == member.getMemberNo()) {
        if (member.okPassword(password)) {
          returnPoints = member.getPoints();
        } else {
          System.out.println("Password is wrong");
        }
      } else {
        System.out.println("Member number is wrong");
      }
    }
    return returnPoints;
  }

  /**
   * A method to add a member to the list.
   *
   * @param pers         Perosnal details
   * @param dateEnrolled Date enrolled
   * @return returns the member number
   */
  public int addMember(Personals pers, LocalDate dateEnrolled) {
    BasicMember basicMember = new BasicMember(findAvailableNo(), pers, dateEnrolled);
    members.put(basicMember.getMemberNo(), basicMember);
    return basicMember.getMemberNo();
  }

  /**
   * A method to register points to a certain member.
   *
   * @param memberNo Member number
   * @param points   amount of points to be registered.
   */
  public void registerPoints(int memberNo, int points) {
    members.get(memberNo).registerPoints(points);
  }

  /**
   * Gets the member based on member number.
   *
   * @param memberNo Member number
   * @return A member with the corresponding member number
   */
  public BonusMember getMember(int memberNo) {
    return members.get(memberNo);
  }

  /**
   * A method which randomly generates member numbers.
   *
   * @return The randomly selected member number
   */
  private int findAvailableNo() {
    Random random = new Random();
    return random.ints(1, 10000)
            .filter(no -> members.get(no) == null)
            .findFirst()
            .getAsInt();
  }

  /**
   * Checks if members can be promoted to silver or gold.
   *
   * @param today The date
   */
  public void checkMembers(LocalDate today) {
    members.values().forEach(member -> {
      if (member.findQualificationPoints(today) == 0) {
        System.out.println("No points");
      } else {
        if (member instanceof BasicMember) {
          if (member.getPoints() >= 30000 && member.getPoints() < 75000) {
            members.put(member.getMemberNo(), new SilverMember(member.getMemberNo(),
                    member.getPersonals(), member.getEnrolledDate(), member.getPoints()));
          } else if (member.getPoints() >= 75000) {
            members.put(member.getMemberNo(), new GoldMember(member.getMemberNo(),
                    member.getPersonals(), member.getEnrolledDate(), member.getPoints()));
          }
        } else if (member instanceof SilverMember) {
          if (member.getPoints() >= 75000) {
            members.put(member.getMemberNo(), new GoldMember(member.getMemberNo(),
                    member.getPersonals(), member.getEnrolledDate(), member.getPoints()));
          }
        }
      }
    });
  }

}
