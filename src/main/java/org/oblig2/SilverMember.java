package org.oblig2;

import java.time.LocalDate;

/**
 * A class for silver members, it inherits from BonusMember.
 */
public class SilverMember extends BonusMember {
  /**
   * A constructor for silver members.
   *
   * @param memberNo     Member number
   * @param personals    Personal details
   * @param enrolledDate Date enrolled
   * @param points       previous points
   */
  public SilverMember(int memberNo, Personals personals, LocalDate enrolledDate, int points) {
    super(memberNo, personals, enrolledDate);
    this.bonuspoints = points;
  }

  /**
   * A function for registering points.
   *
   * @param points Points to be registered
   */
  @Override
  public void registerPoints(int points) {
    super.registerPoints((int) (points * super.getFactorSilver()));
  }
}
