package org.oblig2;

import java.time.LocalDate;

/**
 * A class for gold members, it inherits from BonusMember.
 */
public class GoldMember extends BonusMember {
  /**
   * A constructor for the GoldMember class.
   *
   * @param memberNo     Member number
   * @param personals    Perosnal details
   * @param enrolledDate Date enrolled
   * @param points       Previous points
   */
  public GoldMember(int memberNo, Personals personals, LocalDate enrolledDate, int points) {
    super(memberNo, personals, enrolledDate);
    this.bonuspoints = points;
  }

  /**
   * A function for registering points.
   *
   * @param points Points to be registered
   */
  @Override
  public void registerPoints(int points) {
    super.registerPoints((int) (points * super.getFactorGold()));
  }
}
