package org.oblig2;

import java.time.LocalDate;

/**
 * This is a class for basic member, it inherits from BonusMember.
 */
public class BasicMember extends BonusMember {
  /**
   * Constructor for a basic Member.
   * @param memberNo Member number
   * @param personals Personal details
   * @param enrolledDate The date they enrolled
   */
  public BasicMember(int memberNo, Personals personals, LocalDate enrolledDate) {
    super(memberNo, personals, enrolledDate);
  }
}
