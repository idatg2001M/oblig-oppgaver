package org.oblig4.gui;

import java.time.LocalDate;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.oblig4.MemberArchive;
import org.oblig4.Personals;
import org.oblig4.members.BasicMember;
import org.oblig4.members.BonusMember;
import org.oblig4.members.SilverMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemberArchiveApplication extends Application {
  /**
   * This field contains the member archive.
   */
  MemberArchive members;
  /**
   * This field is used for dialog windows.
   */
  Dialogs dialogs;
  /**
   * The table for showing members.
   */
  TableView<BonusMember> table;
  private Logger logger = LoggerFactory.getLogger(MemberArchiveApplication.class);

  /**
   * A Constructor for the Gui class.
   */
  public MemberArchiveApplication() {
    members = new MemberArchive();
    dialogs = new Dialogs();
    addTestMembers();
  }

  /**
   * This method starts the gui.
   */
  public void startGui() {
    launch();
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    makeTable();
    table.prefHeightProperty().bind(primaryStage.heightProperty());

    VBox pane = new VBox();
    pane.getChildren().add(makeButtons());
    pane.getChildren().add(table);

    Scene scene = new Scene(pane, 800, 500);

    primaryStage.setTitle("Gym member application");
    primaryStage.setScene(scene);
    primaryStage.setMinWidth(450);
    primaryStage.setMinHeight(300);
    primaryStage.show();
  }

  @Override
  public void stop() {
    System.exit(0);
  }

  /**
   * A method which adds two test members.
   */
  public void addTestMembers() {
    members.addMember(new Personals("Ola", "Nordmann", "ola@gmail.no", "nice"),
            LocalDate.of(2020, 3, 1));
    members.addMember(new Personals("TestName", "TestSurname", "TestEmail", "TestPassword"),
            LocalDate.of(2020, 3, 1));
    members.getMembers().values().forEach(member -> member.registerPoints(70000));
  }

  /**
   * A method which creates a table.
   */
  public void makeTable() {
    TableColumn<BonusMember, Integer> memberNoColumn = new TableColumn<>("Member Number");
    memberNoColumn.setMinWidth(200);
    TableColumn<BonusMember, String> nameColumn = new TableColumn<>("Name");
    nameColumn.setMinWidth(200);
    TableColumn<BonusMember, Integer> pointsColumn = new TableColumn<>("Points");
    pointsColumn.setMinWidth(200);
    TableColumn<BonusMember, String> memberTypeColumn = new TableColumn<>("Member Type");
    memberTypeColumn.setMinWidth(200);

    memberNoColumn.setCellValueFactory(new PropertyValueFactory<>("memberNo"));
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
    pointsColumn.setCellValueFactory(new PropertyValueFactory<>("points"));
    memberTypeColumn.setCellValueFactory(cellvalue -> {
      if (cellvalue.getValue() instanceof BasicMember) {
        return new SimpleStringProperty("Basic Member");
      } else if (cellvalue.getValue() instanceof SilverMember) {
        return new SimpleStringProperty("Silver Member");
      } else {
        return new SimpleStringProperty("Gold Member");
      }
    });
    table = new TableView<>();
    table.setItems(getMembers());
    table.getColumns().addAll(memberNoColumn, nameColumn, pointsColumn, memberTypeColumn);
  }

  /**
   * A method which makes buttons for the applicaiton.
   *
   * @return a HBox with the buttons
   */
  public HBox makeButtons() {
    Button btnAdd = new Button("Add New Member");
    Button btnRemove = new Button("Remove a member");
    Button btnUpgrade = new Button("Upgrade members");
    Button btnDetails = new Button("Show details of member");
    Button btnAddPoints = new Button("Add points to a member");
    HBox buttons = new HBox();
    buttons.getChildren().add(btnAdd);
    buttons.getChildren().add(btnRemove);
    buttons.getChildren().add(btnUpgrade);
    buttons.getChildren().add(btnDetails);
    buttons.getChildren().add(btnAddPoints);
    buttons.setPadding(new Insets(10, 10, 10, 10));
    buttons.setSpacing(10);

    btnAdd.setOnAction(env -> buttonAdd());
    btnRemove.setOnAction(env -> buttonRemove());
    btnUpgrade.setOnAction(env -> buttonUpgrade());
    btnDetails.setOnAction(env -> buttonDetails());
    btnAddPoints.setOnAction(env -> buttonAddPoints());
    return buttons;
  }

  /**
   * A method that gets all the members from the member archive.
   *
   * @return an observable list of all the members
   */
  public ObservableList<BonusMember> getMembers() {
    ObservableList<BonusMember> tableMembers = FXCollections.observableArrayList();
    tableMembers.addAll(members.getMembers().values());
    return tableMembers;
  }

  /**
   * This method handles what happens when the add member button is pressed.
   */
  public void buttonAdd() {
    String[] results = dialogs.addMemberButton();
    try {
      members.addMember(new Personals(results[0], results[1],
              results[2], results[3]), LocalDate.now());
    } catch (Exception e) {
      logger.error("Cant add because some fields are empty");
    }
    table.setItems(getMembers());
    table.refresh();
  }

  /**
   * This method handles what happens when the remove member button is pressed.
   */
  public void buttonRemove() {
    if (table.getSelectionModel().isEmpty()) {
      dialogs.notSelected();
      logger.info("No member selected from the table");
    } else {
      int memberNo = table.getSelectionModel().getSelectedItem().getMemberNo();
      if (dialogs.alertDelete(memberNo)) {
        members.getMembers().remove(memberNo);
      }
      table.setItems(getMembers());
    }
  }

  /**
   * This method handles what happens when the upgrade members button is pressed.
   */
  public void buttonUpgrade() {
    members.checkMembers(LocalDate.now());
    table.setItems(getMembers());
  }

  /**
   * This method handles what happens when the show details button is pressed.
   */
  public void buttonDetails() {
    if (table.getSelectionModel().isEmpty()) {
      dialogs.notSelected();
      logger.info("No member selected from the table");
    } else {
      dialogs.showDetails(members.getMember(
              table.getSelectionModel().getSelectedItem().getMemberNo()));
    }
  }

  /**
   * This method handles what happens when the add points button is pressed.
   */
  public void buttonAddPoints() {
    if (table.getSelectionModel().isEmpty()) {
      dialogs.notSelected();
    } else {
      try {
        table.getSelectionModel().getSelectedItem().registerPoints(dialogs.addPoints());
      } catch (IllegalArgumentException e) {
        dialogs.inputNegative();
      }
    }
    table.setItems(getMembers());
    table.refresh();
  }
}
