package org.oblig4.gui;

import java.util.Optional;
import javafx.beans.binding.Bindings;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import org.oblig4.members.BonusMember;
import org.oblig4.members.GoldMember;
import org.oblig4.members.SilverMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Dialogs {
  private Logger logger = LoggerFactory.getLogger(Dialogs.class);

  /**
   * This method handles what alert appears when the add member button is clicked.
   *
   * @return a string array with inputs of the user
   */
  public String[] addMemberButton() {
    Dialog<String[]> dialog = new Dialog<>();
    dialog.setTitle("Add Member");
    dialog.setHeaderText("Enter required information about the new member");
    dialog.setResizable(true);
    GridPane grid = new GridPane();

    TextField fieldFirstName = new TextField();
    TextField fieldLastName = new TextField();
    TextField fieldEmail = new TextField();
    PasswordField fieldPassword = new PasswordField();

    grid.add(new Label("First name:"), 0, 0);
    grid.add(fieldFirstName, 1, 0);
    grid.add(new Label("Last name:"), 0, 1);
    grid.add(fieldLastName, 1, 1);
    grid.add(new Label("Email:"), 0, 2);
    grid.add(fieldEmail, 1, 2);
    grid.add(new Label("Password:"), 0, 3);
    grid.add(fieldPassword, 1, 3);


    dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

    dialog.getDialogPane().lookupButton(ButtonType.OK).disableProperty()
            .bind(Bindings.createBooleanBinding(() -> fieldFirstName.getText().trim().isEmpty()
                            || fieldLastName.getText().trim().isEmpty()
                            || fieldEmail.getText().trim().isEmpty()
                            || fieldPassword.getText().trim().isEmpty(),
                    fieldFirstName.textProperty(),
                    fieldLastName.textProperty(),
                    fieldEmail.textProperty(),
                    fieldPassword.textProperty()
            ));

    dialog.getDialogPane().setContent(grid);
    dialog.showAndWait();
    return new String[]{fieldFirstName.getText(), fieldLastName.getText(),
            fieldEmail.getText(), fieldPassword.getText()};
  }

  /**
   * A method which opens an alert window for confirming deletion of a member.
   *
   * @param memberNo takes the member number of the member which shall be deleted as a parameter
   * @return returns a boolean statement if the member shall be deleted or not.
   */
  public boolean alertDelete(int memberNo) {
    boolean delete = false;
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setContentText("Are you sure you want to delete member with member number: "
            + memberNo);
    alert.setResizable(true);
    Optional<ButtonType> result = alert.showAndWait();
    if (!result.isPresent()) {
      logger.info("No result");
    } else if (result.get() == ButtonType.OK) {
      delete = true;
      logger.info("Deleted");
    } else if (result.get() == ButtonType.CANCEL) {
      logger.info("Not deleted");
    }
    return delete;
  }

  /**
   * Shows the details of a member in dialog pop up window.
   *
   * @param member takes the member as a parameter
   */
  public void showDetails(BonusMember member) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setResizable(true);
    alert.setHeaderText("Member details of: " + member.getName());

    String memberType = "Basic Member";
    if (member instanceof SilverMember) {
      memberType = "Silver Member";
    } else if (member instanceof GoldMember) {
      memberType = "Gold Member";
    }
    GridPane grid = new GridPane();
    grid.add(new Text("Member Number: "), 0, 0);
    grid.add(new Text(Integer.toString(member.getMemberNo())), 1, 0);

    grid.add(new Text("Full Name: "), 0, 1);
    grid.add(new Text(member.getName()), 1, 1);

    grid.add(new Text("Points: "), 0, 2);
    grid.add(new Text(Integer.toString(member.getPoints())), 1, 2);

    grid.add(new Text("Email: "), 0, 3);
    grid.add(new Text(member.getEmail()), 1, 3);

    grid.add(new Text("Member Type: "), 0, 4);
    grid.add(new Text(memberType), 1, 4);

    grid.add(new Text("Date Registered: "), 0, 5);
    grid.add(new Text(member.getEnrolledDate().toString()), 1, 5);
    alert.getDialogPane().setContent(grid);
    alert.show();
  }

  /**
   * A method which shows a text input dialog when the add points button is pressed.
   *
   */
  public int addPoints() {
    TextInputDialog td = new TextInputDialog();
    td.setHeaderText("Add Points");
    td.setContentText("Input amount of points you want to add");
    td.showAndWait();
    int result = 0;
    try {
      if (td.getResult().isEmpty()) {
        logger.info("Input is empty");
      } else {
        result = intCheck(td.getResult());
      }
    } catch (NullPointerException e) {
      logger.info("Cancel pressed");
    }
    return result;
  }

  /**
   * Checks whether an input is a number.
   *
   * @param getResult the user input as a String
   * @return the input as an int
   */
  public int intCheck(String getResult) {
    int result = 0;
    try {
      result = Integer.parseInt(getResult);
    } catch (Exception e) {
      logger.error("Value is not an integer");
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setHeaderText("Error");
      alert.setContentText("Input must be a realistic number");
      alert.show();
    }
    return result;
  }

  /**
   * A method which shows an alert when the user has not selected a member.
   */
  public void notSelected() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setHeaderText("Member not selected");
    alert.setContentText("You have to select a member from the table first");
    alert.setResizable(true);
    alert.show();
    logger.info("No person choosen");
  }

  /**
   * A method which shows an alert when the user input is negative.
   */
  public void inputNegative() {
    logger.error("Value is negative");
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setHeaderText("Error");
    alert.setContentText("Number must be positive");
    alert.show();
  }
}
