package org.oblig4;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.oblig4.members.BasicMember;
import org.oblig4.members.BonusMember;
import org.oblig4.members.GoldMember;
import org.oblig4.members.SilverMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * A class for archiving all the members.
 */
public class MemberArchive {
  private HashMap<Integer, BonusMember> members;
  private Random random;
  private Logger logger = LoggerFactory.getLogger(MemberArchive.class);

  /**
   * A constructor for MemberArchive.
   */
  public MemberArchive() {
    members = new HashMap<>();
    random = new Random();
  }

  /**
   * A methods whichs finds the amount of points based on the memberNo and password.
   *
   * @param memberNo Member number
   * @param password Password
   * @return amount of points the member has
   */
  public int findPoints(int memberNo, String password) {
    int returnPoints = -1;
    for (BonusMember member : members.values()) {
      if (memberNo == member.getMemberNo()) {
        if (member.okPassword(password)) {
          returnPoints = member.getPoints();
        } else {
          logger.info("Password is wrong");
        }
      } else {
        logger.info("Member number is wrong");
      }
    }
    return returnPoints;
  }

  /**
   * A method to add a member to the list.
   *
   * @param pers         Personal details
   * @param dateEnrolled Date enrolled
   * @return returns the member number
   */
  public int addMember(Personals pers, LocalDate dateEnrolled) {
    BasicMember basicMember = new BasicMember(findAvailableNo(), pers, dateEnrolled);
    members.put(basicMember.getMemberNo(), basicMember);
    return basicMember.getMemberNo();
  }

  /**
   * A method to register points to a certain member.
   *
   * @param memberNo Member number
   * @param points   amount of points to be registered.
   */
  public void registerPoints(int memberNo, int points) {
    members.get(memberNo).registerPoints(points);
  }

  /**
   * Gets the member based on member number.
   *
   * @param memberNo Member number
   * @return A member with the corresponding member number
   */
  public BonusMember getMember(int memberNo) {
    return members.get(memberNo);
  }

  /**
   * A method which randomly generates member numbers.
   *
   * @return The randomly selected member number
   */
  private int findAvailableNo() {
    return random.ints(1, 10000)
            .filter(no -> members.get(no) == null)
            .findFirst()
            .getAsInt();
  }

  /**
   * Checks if members can be promoted to silver or gold.
   *
   * @param today The date
   */
  public void checkMembers(LocalDate today) {
    try {
      members.values().forEach(member -> {
        if (member.findQualificationPoints(today) == 0) {
          logger.info("No points");
        } else {
          if (member instanceof BasicMember) {
            if (member.getPoints() >= 30000 && member.getPoints() < 75000) {
              members.put(member.getMemberNo(), new SilverMember(member.getMemberNo(),
                      member.getPersonals(), member.getEnrolledDate(), member.getPoints()));
            } else if (member.getPoints() >= 75000) {
              members.put(member.getMemberNo(), new GoldMember(member.getMemberNo(),
                      member.getPersonals(), member.getEnrolledDate(), member.getPoints()));
            }
          } else if (member instanceof SilverMember && member.getPoints() >= 75000) {
            members.put(member.getMemberNo(), new GoldMember(member.getMemberNo(),
                    member.getPersonals(), member.getEnrolledDate(), member.getPoints()));
          }
        }
      });
    } catch (IllegalArgumentException e) {
      logger.error("Date is invalid");
    }
  }

  public Map<Integer, BonusMember> getMembers() {
    return members;
  }
}
