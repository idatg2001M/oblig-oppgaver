package org.oblig4;

import org.oblig4.gui.MemberArchiveApplication;

public class Main {

  public static void main(String[] args) {
    MemberArchiveApplication memberArchiveApplication = new MemberArchiveApplication();
    memberArchiveApplication.startGui();
  }
}
