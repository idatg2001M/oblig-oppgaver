package org.oblig4.members;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import org.oblig4.Personals;


/**
 * A super class for all members.
 */
public abstract class BonusMember {

  private static final double FACTOR_SILVER = 1.2;
  private static final double FACTOR_GOLD = 1.5;
  private final int memberNo;
  private final Personals personals;
  private final LocalDate enrolledDate;
  protected int bonuspoints = 0;

  /**
   * A constructor for BonusMember.
   *
   * @param memberNo     Member number
   * @param personals    Personal details
   * @param enrolledDate Date enrolled
   */
  public BonusMember(int memberNo, Personals personals, LocalDate enrolledDate) {
    if (personals == null || enrolledDate == null) {
      throw new IllegalArgumentException();
    } else {
      this.memberNo = memberNo;
      this.personals = personals;
      this.enrolledDate = enrolledDate;
    }
  }

  public double getFactorSilver() {
    return FACTOR_SILVER;
  }

  public double getFactorGold() {
    return FACTOR_GOLD;
  }

  public int getMemberNo() {
    return memberNo;
  }

  public Personals getPersonals() {
    return personals;
  }

  public LocalDate getEnrolledDate() {
    return enrolledDate;
  }

  public int getPoints() {
    return bonuspoints;
  }

  public String getName() {
    return personals.getFirstname() + " " + personals.getSurname();
  }

  public String getEmail() {
    return personals.getEMailAddress();
  }

  /**
   * A method for checking if there has been more than 365 after they signed up.
   *
   * @param date Date to compare to the sign up date.
   * @return the amount of points the member gets
   */
  public int findQualificationPoints(LocalDate date) {
    if (date == null) {
      throw new IllegalArgumentException();
    }
    long daysBetween = ChronoUnit.DAYS.between(this.enrolledDate, date);
    int points = 0;
    if (daysBetween < 365) {
      points = getPoints();
    }
    return points;
  }

  /**
   * Checks if the password is correct.
   *
   * @param password Password
   * @return true if password is correct, otherwise false.
   */
  public boolean okPassword(String password) {
    return personals.okPassword(password);
  }

  /**
   * A method to register points to a member.
   *
   * @param points Amount of points to be registered.
   */
  public void registerPoints(int points) {
    if (points < 0) {
      throw new IllegalArgumentException("Points cannot be less than zero");
    }
    this.bonuspoints += points;
  }
}
