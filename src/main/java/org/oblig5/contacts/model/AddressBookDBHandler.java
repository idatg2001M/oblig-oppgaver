package org.oblig5.contacts.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.Collection;
import java.util.Iterator;

public class AddressBookDBHandler implements AddressBook {
  private EntityManagerFactory efact;

  public AddressBookDBHandler() {
    efact = Persistence.createEntityManagerFactory("addressbook-pu");
  }

  @Override
  public void addContact(ContactDetails contact) {
    EntityManager em = efact.createEntityManager();
    try {
      em.getTransaction().begin();
      em.merge(contact);
      em.flush();
      em.getTransaction().commit();
    } finally {
      em.close();
    }
  }

  @Override
  public void removeContact(String phoneNumber) {
    EntityManager em = efact.createEntityManager();
    try {
      em.getTransaction().begin();
      em.remove(em.find(ContactDetails.class, phoneNumber));
      em.getTransaction().commit();
    } finally {
      em.close();
    }
  }

  @Override
  public Collection<ContactDetails> getAllContacts() {
    EntityManager em = efact.createEntityManager();
    Collection<ContactDetails> contactList = null;

    String jdbcQuery = "SELECT c FROM ContactDetails c";
    Query databaseQuery = em.createQuery(jdbcQuery);

    contactList = databaseQuery.getResultList();
    em.close();
    return contactList;
  }

  @Override
  public Iterator<ContactDetails> iterator() {
    return getAllContacts().iterator();
  }

  @Override
  public void close() {
    efact.close();
  }
}
