package org.oblig5.zoo;

import java.time.LocalDate;

public class ZooClient {

  private static WildAnimalFactory wildAnimalFactory = WildAnimalFactory.getWildAnimalFactory();

  public static void main(String[] args) {
    ScandinavianWildAnimal femaleWolf = wildAnimalFactory.newFemaleWolf(LocalDate.of(2015,4,29), "Ulla",
            LocalDate.of(2015,2,26), "Innhegning 2, Skandinaviske rovdyr", 0);

    System.out.println(femaleWolf.getAddress());
    System.out.println(femaleWolf.getAge());

  }
}
