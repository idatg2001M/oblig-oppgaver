package org.oblig1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class Deck {

  private List<Card> cards;

  public Deck() {
    cards = new ArrayList<>();
  }

  interface Interface {
    ArrayList<Card> chosenCards(int number);
  }

  /**
   * A method which creates a full deck of cards.
   */
  public void createDeck() {
    for (int i = 1; i < 14; i++) {
      cards.add(new Card('S', i));
      cards.add(new Card('H', i));
      cards.add(new Card('D', i));
      cards.add(new Card('C', i));
    }
  }

  /**
   * A method which takes a number of random cards from the deck.
   *
   * @param number number of cards
   * @return a list of cards
   */
  public List<Card> assign(int number) {
    Random rand = new Random();
    return rand
            .ints(number, 0, cards.size())
            .mapToObj(i -> cards.get(i))
            .collect(Collectors.toList());
  }

  /**
   * Main function.
   *
   * @param args command arguments
   */
  public static void main(String[] args) {
    Deck deck = new Deck();
    deck.createDeck();
    //Prints out the details of all the cards with the suit Spades
    System.out.println("---------- c ---------- ");
    deck.assign(5).stream()
            .filter(card -> card.getSuit() == 'S')
            .forEach(card -> System.out.println(card.getDetails()));

    //Adds all cards with the hearts suit to a new ArrayList
    System.out.println("---------- d ----------");
    ArrayList<Card> heartList = deck.assign(5).stream()
            .filter(card -> card.getSuit() == 'H')
            .collect(Collectors.toCollection(ArrayList::new));
    heartList.forEach(card -> System.out.println(card.getDetails()));

    //Maps the cards with their suit, and puts them in an ArrayList
    System.out.println("---------- e ----------");
    ArrayList<Character> suitDeck = deck.assign(5).stream()
            .map(Card::getSuit)
            .collect(Collectors.toCollection(ArrayList::new));
    System.out.println(suitDeck);

    //Gives the sum of the cards
    System.out.println("---------- f ----------");
    int sum = deck.assign(5).stream()
            .map(Card::getFace)
            .reduce(0, Integer::sum);
    System.out.println(sum);

    //Checks if one of the cards is a Queen of Spades
    System.out.println("---------- g ----------");
    boolean doesQueenSpadesExist = deck.assign(5).stream()
            .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    System.out.println(doesQueenSpadesExist);

    //Prints out whether there is a flush.
    System.out.println("---------- h ----------");
    Map<Character, Long> count = deck.assign(7)
            .stream()
            .collect(Collectors.groupingBy(Card::getSuit, Collectors.counting()));
    System.out.println(count.values().stream().anyMatch(i -> i >= 5L));
  }
}
