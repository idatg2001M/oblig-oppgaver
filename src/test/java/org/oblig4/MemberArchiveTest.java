package org.oblig4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oblig4.members.BasicMember;
import org.oblig4.members.GoldMember;
import org.oblig4.members.SilverMember;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * A test class used to test mainly the checkMembers function. It checks whether the member can
 * be upgraded to silver or gold membership.
 */
class MemberArchiveTest {
  private MemberArchive memberArchive;
  private int memberNo;

  @BeforeEach
  void setUp() {
    LocalDate testDate = LocalDate.of(2009, 1, 1);
    Personals ole = new Personals("Ole", "Nordmann", "ole@nice.no", "nice");
    memberArchive = new MemberArchive();
    memberNo = memberArchive.addMember(ole, testDate);
  }

  /**
   * Tests if the member can be upgraded to silver
   */
  @Test
  void testUpgradeSilver() {
    memberArchive.registerPoints(memberNo, 40000);
    memberArchive.checkMembers(LocalDate.of(2009, 2, 2));
    System.out.println("Test 1: Checking if Ole is a silver member.");
    assertTrue(memberArchive.getMember(memberNo) instanceof SilverMember);
  }

  /**
   * Tests if the member can be upgraded to gold
   */
  @Test
  void testUpgradeGold() {
    memberArchive.registerPoints(memberNo, 75000);
    memberArchive.checkMembers(LocalDate.of(2009, 2, 2));
    System.out.println("Test 2: Checking if Ole is a gold member.");
    assertTrue(memberArchive.getMember(memberNo) instanceof GoldMember);
  }

  /**
   * Checks if the member is not upgraded when he should'nt
   */
  @Test
  void testNotUpgraded() {
    memberArchive.registerPoints(memberNo, 10000);
    memberArchive.checkMembers(LocalDate.of(2009, 2, 2));
    System.out.println("Test 3: Checking if Ole is not upgraded");
    assertTrue(memberArchive.getMember(memberNo) instanceof BasicMember);
  }
}
